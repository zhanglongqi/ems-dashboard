import {NgModule}      from '@angular/core';
import {CommonModule}  from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgaModule} from '../../theme/nga.module';
import {Ng2SmartTableModule} from 'ng2-smart-table';

import {routing}       from './uc.routing';
import {UnitCommitment} from './uc.component';
import {BasicTables} from './components/basicTables/basicTables.component';
import {BasicTablesService} from './components/basicTables/basicTables.service';
import {ResponsiveTable} from './components/basicTables/components/responsiveTable';
import {StripedTable} from './components/basicTables/components/stripedTable';
import {BorderedTable} from './components/basicTables/components/borderedTable';
import {HoverTable} from './components/basicTables/components/hoverTable';
import {CondensedTable} from './components/basicTables/components/condensedTable';
import {ContextualTable} from './components/basicTables/components/contextualTable';
import {SmartTables} from './components/smartTables/smartTables.component';
import {SmartTablesService} from './components/smartTables/smartTables.service';
import {Energy} from './energy/energy.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    Ng2SmartTableModule,
  ],
  declarations: [
    UnitCommitment,
    BasicTables,
    HoverTable,
    BorderedTable,
    CondensedTable,
    StripedTable,
    ContextualTable,
    ResponsiveTable,
    SmartTables,
    Energy
  ],
  providers: [
    BasicTablesService,
    SmartTablesService,
  ]
})
export class UnitCommitmentModule {
}
