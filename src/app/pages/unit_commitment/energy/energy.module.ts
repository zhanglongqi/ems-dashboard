/**
 * Created by longqi on 2/Mar/17.
 */
import {NgModule}      from '@angular/core';
import {CommonModule}  from '@angular/common';
import {Energy} from './energy.component';
import {routing} from './energy.routing';

@NgModule({
  imports: [
    CommonModule,
    routing
  ],
  declarations: [
    Energy
  ]
})
export class EnergyModule {
}
