/**
 * Created by longqi on 2/Mar/17.
 */
import { Routes, RouterModule }  from '@angular/router';
import { Energy } from './energy.component';

const routes: Routes = [
  {
    path: '',
    component: Energy
  }
];

export const routing = RouterModule.forChild(routes);
