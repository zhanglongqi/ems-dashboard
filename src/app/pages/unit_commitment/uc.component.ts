import {Component} from '@angular/core';

@Component({
  selector: 'unit_commitment',
  template: `<router-outlet></router-outlet>`
})
export class UnitCommitment {

  constructor() {
  }
}
