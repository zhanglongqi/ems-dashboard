/**
 * Created by longqi on 2/Mar/17.
 */
import {Routes, RouterModule}  from '@angular/router';

import {UnitCommitment} from './uc.component';
import {BasicTables} from './components/basicTables/basicTables.component';
import {SmartTables} from './components/smartTables/smartTables.component';
import {Energy} from './energy/energy.component';
// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: UnitCommitment,
    children: [
      {path: 'energy', component: Energy},
      {path: 'reserve', component: SmartTables},
      {path: 'resource_scheduling', component: BasicTables}
    ]
  }
];

export const routing = RouterModule.forChild(routes);
